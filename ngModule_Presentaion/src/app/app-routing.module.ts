import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RootLevelComponent } from './root-level/root-level.component';
import { ModuleLevelComponent } from './customer-dashboard/module-level/module-level.component';
const routes: Routes = [
  {
    path: 'customers',
    loadChildren: './customers/customers.module#CustomersModule'
  },
  {
    path: 'orderlist',
    loadChildren: './orders/orders.module#OrdersModule'
  },
  {
    path: 'rootLevel',
    component: RootLevelComponent
  },
  {
    path: 'moduleLevel',
    component: ModuleLevelComponent
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}
