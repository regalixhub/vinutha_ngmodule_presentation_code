import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CustomerDashboardComponent } from './customer-dashboard/customer-dashboard.component';
import { HighlightDirective } from './highlight.directive';
import { ExponentialStrengthPipe } from './exponential-strength.pipe';
import { ModuleLevelComponent } from './module-level/module-level.component';

@NgModule({
  imports: [CommonModule, FormsModule],
  exports: [
    CustomerDashboardComponent,
    ModuleLevelComponent,
    HighlightDirective,
    ExponentialStrengthPipe
  ],
  declarations: [
    CustomerDashboardComponent,
    HighlightDirective,
    ExponentialStrengthPipe,
    ModuleLevelComponent
  ]

})
export class CustomerDashboardModule {}
