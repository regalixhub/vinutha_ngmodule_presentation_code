import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customer-dashboard',
  templateUrl: './customer-dashboard.component.html',
  styleUrls: ['./customer-dashboard.component.css']
})

export class CustomerDashboardComponent implements OnInit {

  constructor() {}

  birthday = new Date(1988, 3, 15); // April 15, 1988

  power = 5;
  factor = 1;

  ngOnInit() {}
}
