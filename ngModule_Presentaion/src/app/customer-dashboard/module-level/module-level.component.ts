import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-module-level',
  templateUrl: './module-level.component.html',
  styleUrls: ['./module-level.component.css']
})

export class ModuleLevelComponent implements OnInit {

  constructor(private customerService: CustomerService) {}

  birthday = new Date(1988, 3, 15); // April 15, 1988

  power = 5;
  factor = 1;

  ngOnInit() {
    this.factor = this.customerService.factor;
  }

  increamentFactor() {
    this.customerService.factor++;
    this.factor = this.customerService.factor;
  }
}
